// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// tossacoin --
//   Prints "Head" or "Tail" depending on the simulation.
//   Returns 1 for "Head", 0 for "Tail"
function face = tossacoin ( )
  face = floor ( 2 * rand() );
  if ( face == 1 ) then
    mprintf ( "Head\n" )
  else
    mprintf ( "Tail\n" )
  end
endfunction

// Toss a coin 4 times
rand("seed",0)
face = tossacoin ();
face = tossacoin ();
face = tossacoin ();
face = tossacoin ();



// Compute the exact probability of getting exactly 5 heads 
// in 10 toss of a coin.
n = 10
j = 5
p = 0.5
q = 1-p
pc = nchoosek(n,j) * p^j * q^(n-j)

// Simulate the process.
rand("seed",0)
nb = 10000;
success = 0;
for i = 1:nb
  faces = floor ( 2 * rand(1,10) );
  nbheads = sum(faces);
  if ( nbheads == 5 ) then
    success = success + 1;
  end
end
pc = success / nb

// tossingcoin --
//   Returns the probability of getting j heads in 10 tosses of a coin.
//   Uses nb experiments and make an average.
function p = tossingcoin ( j , nb )
  success = 0;
  for i = 1:nb
    faces = floor ( 2 * rand(1,10) );
    nbheads = sum(faces);
    if ( nbheads == j ) then
      success = success + 1;
    end
  end
  p = success / nb
endfunction

rand("seed",0)
counter(1:11) = 0;
for j = 0:10
  p = tossingcoin ( j , 10000);
  counter(j+1) = p;
  mprintf("P(j=%d)=%f\n",j,p);
end
bar(counter)

 
