// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// A simple test
m = 100; k = 30; n = 10;
hygepdf_naive ( 5 , m , k , n )
hygepdf ( 5 , m , k , n )

// Expected result = 1.65570E-��10
m=1030; k=500; n=515;
hygepdf_naive ( 200 , m , k , n )
hygepdf ( 200 , m , k , n )


