// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// Returns a (nbrows x nbcols) matrix of integers in the range 0,m-1
function ri = generateInRange0M1 ( m , nbrows , nbcols )
  ri = floor(rand( nbrows , nbcols ) * m)
endfunction 
r = generateInRange0M1 ( 5 , 10 , 10 )
// Check uniformity
r = generateInRange0M1 ( 5 , 100 , 100 );
counter = zeros(1,5);
for i = 1:100
for j = 1:100
  k = r(i,j);
  counter(k+1) = counter(k+1) + 1;
end
end
counter = counter / 10000;
counter
bar(counter)
xs2png(0,"random_0_4.png")

mean(ri)
// Returns a matrix of integers in the range m1,m2
function ri = generateInRangeM12 ( m1 , m2 , nr , nc )
  f = m2 - m1 + 1
  ri = floor(rand( nr , nc ) * f) + m1
endfunction 
ri = generateInRangeM12 ( 5 , 9 , 10 , 10 )
ri = generateInRangeM12 ( 5 , 9 , 1000 , 1000 );
mean(ri)
// Returns a matrix of integers in the range 1,m
function ri = generateInRange1M ( m , nr , nc )
  ri = ceil(rand( nr , nc ) * m)
endfunction 
ri = generateInRange1M ( 5 , 10 , 10 )
ri = generateInRange1M ( 5 , 1000 , 1000 );
mean(ri)

