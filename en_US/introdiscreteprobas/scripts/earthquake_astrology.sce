// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.



// The number of days in three years
m = 1096
// The number of days selected 
n = 169
// The number of earthquake days in three years
k = 196
// The number of earthquake days selected
x = 33
// The probability of picking 169 days, where 33 are earthquakes.
p = hypergeometric ( x , m , k , n )

// Plot the distribution
xdata = zeros(n+1);
pdata = zeros(n+1);
for i = 0:n
  xdata(i+1) = i;
  pdata(i+1) = hypergeometric ( i , m , k , n );
end
plot(xdata,pdata)
f=gcf();
f.children.title.text="Probability of random predictions";
f.children.x_label.text="Number of earthquakes";
f.children.y_label.text="Probability";

