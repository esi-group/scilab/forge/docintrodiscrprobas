// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.



// Compute the birthday problem.

function p = twobirthday_verynaive ( n , d )
  p = 1 - factorial ( d )./factorial( d - n ) ./ d^n
endfunction

n = (1:5)';
[n twobirthday_verynaive(n,365)]

// twobirthday --
//   Returns the probability that two persons have the 
//   same birthday.
// d : the number of days in the period (example:d=365)
function p = twobirthday_naive ( n , d )
  p = 1 - permutations ( d , n ) ./ d^n
endfunction

n = (1:5)';
[n twobirthday_naive(n,365)]

n = (20:25)';
[n twobirthday_naive(n,365)]

// Practical use: compute the number of persons 
// in the group so that the probability of having two 
// persons with the same birth day and hour is greater than 0.5

n = (20:25)';
[n twobirthday_naive(n,365*24)]

// Oups

// Problem: for large values of n, the formula fails
twobirthday_naive(100,365*24)
permutations ( 365*24 , 100 )

// Less naive formula
function p = twobirthday_lessnaive ( n , d )
  q = exp(permutationslog ( d , n ) - n * log(d))
  p = 1 - q
endfunction

twobirthday_lessnaive(100,365*24)

n = 1;
while ( %t )
  p = twobirthday_lessnaive ( n , 365*24 );
  if ( p > 0.5 ) then
    mprintf("n=%d, p=%e\n",n,p)
    break
  end
  n = n + 1;
end

// What is the probability of having two persons 
// with the same birth day and hour in a group of 500 persons ?
p = twobirthday_lessnaive ( 500 , 365*24 )


// Problem: we have lost 6 significant digits for p

// Robust and accurate formula
function [ p , q ] = twobirthday ( n , d )
  q = exp(permutationslog ( d , n ) - n * log(d))
  p = 1 - q
endfunction

[p,q] = twobirthday ( 500 , 365*24 );
mprintf("p=%.17e\n", p)
mprintf("q=%.17e\n", q)
// Exact result by Mathematica: (365*24)!/(365*24-500)!/((365*24)^500)
z = 4.94591797640207720e-7
// Relative error: 8.e-12
mprintf("%e",abs(q-z)/z)

