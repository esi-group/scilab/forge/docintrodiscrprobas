// Copyright (C) 2009 - 2010 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.


// Drawback: does not accept matrices of integers.
function f = factorial_naive ( n )
  f = prod(1:n)
endfunction

// A simplified version of the factorial function in Scilab
// Notice that n is, at most, 170.
// Therefore 1:max(n) is, at most, an array with 170 entries.
function f = factorialScilab ( n )
  n(n==0)=1
  t = cumprod(1:max(n))
  v = t(n(:))
  f = matrix(v,size(n))
endfunction

// Uses gamma function
function f = myfactorial ( n )
  f = gamma ( n + 1 )
  // If the input was integer, returns also an integer.
  if ( and(round(n)==n) ) then
    f = round ( f )
  end
endfunction


// Test the built-in factorial function.
n = (0:10)
plot ( n , factorial(n) , "b-o" )

// Make a table of values for factorial function.
n = (0:7)';
[n factorial(n)]

// Drawback : does not work for large arrays
factorial(1.e10)



// compare the three approaches
n = (0:30)';
[n factorial(n) myfactorial(n) factorialScilab(n)]

// See the need for rounding
n = (0:30)';
[n factorial(n) myfactorial(n) gamma(n+1)]

myfactorial(11) 
gamma(12)

// Good point : works even for large arrays
myfactorial(1.e10)

n = 1000;
//x = ceil(rand(n,n) * 170);
// This is a worst case for factorial, since 1:170 is 
// formed, while only one call to the gamma function is required.
x = 170;

tt = 0;
for i = 1 : 10
tic;
myfactorial ( x );
tt = tt + toc();
end
mprintf("Average Myfactorial: %f\n",tt)

tt = 0;
for i = 1 : 10
tic;
factorial ( x );
tt = tt + toc();
end
mprintf("Average Factorial: %f\n",tt)

// Test with hypermatrix
n(1:2,1:2,1,1,1:2) = 170
max(n)


