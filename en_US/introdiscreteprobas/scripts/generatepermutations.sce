// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// Using perms, if n is not large
n = 4
p = perms ( (1:n)' )
j = floor ( rand ( ) * n) + 1
v = p(j,:)

// Using grand
Y = grand ( 1 , 'prm' , (1:10)' )

// Shuffle the given row array. 
function array = genperm ( array )
  n = length(array)
  for i = 1:n
    // Get a random number between i and n
    iwhich = floor ( rand ( ) * (n - i + 1)) + i
    elt = array(iwhich)
    array(iwhich) = array(i)
    array(i) = elt
  end
endfunction

