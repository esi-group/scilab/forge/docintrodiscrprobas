// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// 
// binopdf --
//   Returns the Binomial probability density function.
//   Consider a Bernoulli process with probability pb 
//   and n trials. 
//   Returns the probability p that there is exactly x successes.
//   We assume that qb is a positive number such that pb+qb = 1.
//   The role of qb is to allow the an accurate computation 
//   for small values of pb (i.e. with p smaller than %eps).
function p = binopdf ( x , n , pb , qb )
  plog = nchooseklog(n,x) + x .* log(pb) + (n-x) * log(qb)
  p = exp ( plog )
endfunction

function p = binopdf_lessnaive ( x , n , pb , qb )
  p = nchoosek(n,x) .* pb^x .* qb^(n-x)
endfunction

function p = binopdf_naive ( x , n , pb )
  qb = 1 - pb
  p = nchoosek(n,x) * pb^x * qb^(n-x)
endfunction


