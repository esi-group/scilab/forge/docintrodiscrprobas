// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.


// See the discussion at :
// http://bugzilla.scilab.org/show_bug.cgi?id=7670
// http://en.wikipedia.org/wiki/Pascal_matrix

exec("nchoosek.sci");

///////////////////////////////////////////////////////////

// pascallow_naive --
//   Returns Pascal's lower triangular matrix of order n

//    1    0     0     0    0  
//    1    1     0     0    0  
//    1    2     1     0    0  
//    1    3     3     1    0  
//    1    4     6     4    1  

//   Naive implementation : uses two loops.
function c = pascallow_naive (n)
  c = zeros(n,n)
  for i = 1:n
    for j = 1:i
      c(i,j) = nchoosek (i-1,j-1)
    end
  end
endfunction

// Less naive implementation : uses one loop.
// Fills the matrix row by row.
// This is not a good choice, since the memory in Scilab
// is column by column.
function c = pascallow_lessnaiverow (n)
  c = zeros(n,n)
  for i = 1:n
    c(i,1:i) = nchoosek (i-1,(1:i)-1)
  end
endfunction
// An implementation column by column (this should be better).
function c = pascallow_lessnaivecol (n)
  c = zeros(n,n)
  for i = 1:n
    c(i:n,i) = nchoosek ((i-1:n-1)',i*ones(n-i+1,1)-1)
  end
endfunction
// An implementation based on the exponential
function c = pascallow_expm ( n )
  if ( n == 1 ) then
    c = 1
  else
    c = expm(diag(1:(n-1),-1))
  end
endfunction
// pascallow --
//   Returns Pascal's lower triangular matrix of order n
//   No loop.
function c = pascallow_noloop (n)
  N = tril((0:n-1)' * ones(1,n))
  K = tril(((0:n-1)' * ones(1,n))')
  c = zeros(n,n)
  c(1:n,1) = 1
  nz = find(N<>0 & K<>0)
  if ( nz <> [] ) then
    c(nz) = nchoosek (N(nz),K(nz))
  end
endfunction

// Uses a row by row implementation with 1-loop.
// Calixte Denizet.
// Same implementation was suggested by Samuel Gougeon.
function c = pascallow_re ( n )
   c = eye(n,n)
   c(:,1) = ones(n,1)
   for i = 2:(n-1)
      c(i+1,2:i) = c(i,1:(i-1))+c(i,2:i)
   end
endfunction

// Pascal up matrix.
// Column by column version
function c = pascalup_col (n)
   c = eye(n,n)
   c(1,:) = ones(1,n)
   for i = 2:(n-1)
      c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
   end
endfunction


function [t,msg] = benchalgo ( name , fun , args , kmax )
  for k = 1 : kmax
    timer()
    fun ( args )
    t(k) = timer()
  end
  msg = msprintf("%s: mean=%f, min=%f, max=%f\n",name,mean(t),min(t),max(t))
  mprintf("%s\n",msg)
endfunction

for n = 1 : 10
  disp("pascallow_naive")
  pascallow_naive (n)
  disp("pascallow_lessnaiverow")
  pascallow_lessnaiverow (n)
  disp("pascallow_lessnaivecol")
  pascallow_lessnaivecol (n)
  disp("pascallow_expm")
  pascallow_expm (n)
  disp("pascallow_noloop")
  pascallow_noloop (n)
  disp("pascalup_col")
  pascalup_col (n)'
end

N = 200;
benchalgo ( "pascallow_naive" , pascallow_naive , N , 10 );
benchalgo ( "pascallow_lessnaiverow" , pascallow_lessnaiverow , N , 10 );
benchalgo ( "pascallow_lessnaivecol" , pascallow_lessnaivecol , N , 10 );
benchalgo ( "pascallow_expm" , pascallow_expm , N , 10 );
benchalgo ( "pascallow_noloop" , pascallow_noloop , N , 10 );
benchalgo ( "pascallow_re" , pascallow_re , N , 10 );
benchalgo ( "pascalup_col" , pascalup_col , N , 10 );

// pascalup_col:           mean=0.003120, min=0.000000, max=0.031200
// pascallow_re:           mean=0.007800, min=0.000000, max=0.046800
// pascallow_lessnaiverow: mean=0.017160, min=0.015600, max=0.031200
// pascallow_lessnaivecol: mean=0.023400, min=0.015600, max=0.046800
// pascallow_noloop:       mean=0.062400, min=0.015600, max=0.109201
// pascallow_naive:        mean=0.712925, min=0.702004, max=0.733205
// pascallow_expm:         mean=2.263575, min=2.106013, max=2.386815



///////////////////////////////////////////////////////////

// pascalsym --
//   Returns Pascal's symmetric matrix of order n
//   Naive implementation : uses two loops.
function c = pascalsym_naive (n)
  c = zeros(n,n)
  for i = 1:n
    for j = 1:n
      m = i+j-2
      k = i-1
      c(i,j) = nchoosek (m,k)
    end
  end
endfunction

pascalsym_naive (5)

//    1    1    1     1     1   
//    1    2    3     4     5   
//    1    3    6     10    15  
//    1    4    10    20    35  
//    1    5    15    35    70  

// pascalsym --
//   Returns Pascal's matrix of order n
//   Less naive implementation : uses one loop.
function c = pascalsym_lessnaive (n)
  c = zeros(n,n)
  for i = 1:n
    c(i,1:n) = nchoosek (i+(1:n)-2,i-1)
  end
endfunction
pascalsym_lessnaive (5)

// pascalsym --
//   Returns Pascal's matrix of order n
//   No loop.
function P = pascalsym_noloop (n)
  K = (0:n-1)' * ones(1,n)
  N = K+K'
  P = nchoosek (N,K)
endfunction

pascalsym_noloop (5)

// pascalsym --
//   Returns Pascal's matrix of order n
//   Uses pascalup_col
function P = pascalsym_fromup (n)
  U = pascalup_col ( n )
  P = U' * U
endfunction

pascalsym_fromup (5)

N = 100;
benchalgo ( "pascalsym_lessnaive" , pascalsym_lessnaive , N , 10 ); // 0.018026
benchalgo ( "pascalsym_noloop" , pascalsym_noloop , N , 10 ); // 0.019027
benchalgo ( "pascalsym_naive" , pascalsym_naive , N , 10 ); // 0.600864
benchalgo ( "pascalsym_fromup" , pascalsym_fromup , N , 10 ); // 0.005007

N = 500;
benchalgo ( "pascalsym_lessnaive" , pascalsym_lessnaive , N , 10 ); // 0.205295
benchalgo ( "pascalsym_noloop" , pascalsym_noloop , N , 10 ); // 0.314452
benchalgo ( "pascalsym_fromup" , pascalsym_fromup , N , 10 ); // 0.558804
benchalgo ( "pascalsym_naive" , pascalsym_naive , N , 10 ); // 14.720167

///////////////////////////////////////////////////////////

// The link between low, up and symetric Pascal matrices

L = pascallow_naive ( 5 );
U = L';
S = pascalsym_naive ( 5 );
L * U - S



