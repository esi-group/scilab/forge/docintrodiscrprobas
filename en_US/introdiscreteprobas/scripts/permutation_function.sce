// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

function p = permutations_verynaive ( n , j )
  p = factorial(n)./factorial(n-j)
endfunction

// Let us check that it works well for regular values of n and j
n = [5 5 5 5 5 5]';
j = [0 1 2 3 4 5]';
p = permutations_verynaive ( 5 , j );
[n j p]

// There is nothing to do about it.
permutations_verynaive ( 171 , 171 )

// Oups, no more memory: there is certainly something to 
// do about it!
permutations_verynaive ( 1.e7 , 1.e7 )

// This is very bad...
permutations_verynaive ( 171 , 0 )


// permutations --
//   Returns the number of permutations of j objects chosen from n objects.
// Drawback: generates large array.
// Drawback: does not work on matrices.
function p = permutations_naive ( n , j )
  p = prod ( n-j+1 : n )
endfunction

n = 5;
for j = 0 : 5
  p = permutations_naive ( n , j );
  disp([n j p]);
end

// There is nothing to do about it.
permutations_naive ( 171 , 171 )

// This is better
permutations_naive ( 171 , 0 )

// Oups, no more memory: there is certainly something to 
// do about it!
permutations_naive ( 1.e7 , 1.e7 )

// permutations --
//   Returns the number of permutations of j objects chosen from n objects.
// Does work on matrices.
function p = permutations ( n , j )
  p = exp(gammaln(n+1)-gammaln(n-j+1));
  if ( and(round(n)==n) & and(round(j)==j) ) then
    p = round ( p )
  end
endfunction
permutations ( 5 , 0 )
permutations ( 5 , 1 )
permutations ( 5 , 2 )
permutations ( 5 , 3 )
permutations ( 5 , 4 )
permutations ( 5 , 5 )

permutations ( 171 , 171 )
// Inf is still a better result than "no more memory"
permutations ( 1.e7 , 1.e7 )

for j = 1:5
  p = permutations ( 5 , j );
  check = factorial(5)/ factorial(5-j);
  mprintf("(%d)_%d = %d (%d)\n",5,j,p,check);
end

n = [5 5 5 5 5 5]';
j = [0 1 2 3 4 5]';
[n j permutations(n,j)]

// permutationslog --
//   Returns the logarithm of the number of permutations of j objects chosen from n objects.
// Does work on matrices.
function plog = permutationslog ( n , j )
  plog = gammaln(n+1)-gammaln(n-j+1);
endfunction

