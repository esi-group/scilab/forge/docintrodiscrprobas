// Copyright (C) 2009 - Michael Baudin
// This file must be used under the terms of the GNU LGPL license.

// One woman has a 20% chance of getting pregnant 
// each cycle. What is the probability of getting 
// pregnant after n cycles ?

// Returns the number of trials of a Bernouilli
// process before success, where p is the probability 
// for success at each trial.
function n = trials ( p , verbose )
  if exists("verbose","local")==0 then
    verbose = 0
  end
  n = 1
  while ( %t )
    r = rand()
    if ( verbose == 1 ) then
      mprintf("Trial #%d, r = %f\n" , n , r)
    end
    if ( r < p ) then
      if ( verbose == 1 ) then
        mprintf("Success !\n")
      end
      break
    end
    n = n + 1
  end
endfunction
// Returns the empirical cumulated density function of the 
// given data, at given points x.
function cdf = discrete_cdf ( x , data )
  nd = length ( data );
  nx = length ( x );
  cdf = sum(data*ones(1,nx) <= ones(nd,1) * x , "r" )/nd;
endfunction

// Draw the histogram
rand("seed",0);
p = 0.2;
nsim = 10000;
for i = 1 : nsim
  n(i) = trials(p);
end
histplot ( 10 , n );
mprintf ( "Min:%d\n",min(n));
mprintf ( "Max:%d\n",max(n));
mprintf ( "Average:%d\n",mean(n));
mprintf ( "StdVar:%d\n",st_deviation(n));

// Compute the empirical cumulated density function
nx = 100;
x = linspace ( min(n) , max(n) , nx );
for i = 1 : nx
  ecdf ( i ) = length (find ( n <= x(i) ))/nsim;
end
scf();
plot ( x , ecdf );

// Vectorized
ecdf = discrete_cdf ( x , n );


