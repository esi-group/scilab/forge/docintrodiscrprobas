// Copyright (C) 2009 - DIGITEO - Michael Baudin
// Copyright (C) 2009 - DIGITEO - Yann Collette 
// Copyright (C) 2009 - DIGITEO - Pierre Lando 
//
// This file must be used under the terms of the GNU LGPL license.

function R = combinatevectors ( varargin ) 
  //   Returns the all the combinations of the given vectors. 
  //
  // Calling Sequence
  //   R = combinatevectors ( x1 , x2 )
  //   R = combinatevectors ( x1 , x2 , x3 )
  //   R = combinatevectors ( x1 , x2 , x3 , x4 )
  //   R = combinatevectors ( x1 , x2 , x3 , x4 , ... )
  //
  // Arguments 
  //   x1 , x2 , x3 , x4 , ... : vectors of doubles which all have the same number of nx elements 
  //   R : na x (nx)! matrix of doubles
  //
  // Description
  //   Generates all the nx! combinations of the given vectors.
  //   The size of the generated matrix can be large when the 
  //   size of the arguments is large : it grows as fast as the factorial function.
  //   The only loop is performed on the number of arguments.
  //   The implementation does not use loops on the elements to combine, which leads to a 
  //   fast (but tricky) implementation of this algorithm.
  //   The vectorization is based on the use of the Kronecker product ".*.".
  //
  // Examples
  // // Example #1 
  // x = [1 2 3]; 
  // y = [4 5 6]; 
  // expected = [
  //   1 1 1 2 2 2 3 3 3 
  //   4 5 6 4 5 6 4 5 6 
  // ]
  // // See that the combinations are computed column by column, in this case.
  // combinatevectors ( x , y ) 
  // // Example #2 
  // x = [1 2 3]; 
  // y = [4 5 6]; 
  // z = [7 8 9]; 
  // expected = [
  //   1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3 3 
  //   4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6 4 4 4 5 5 5 6 6 6 
  //   7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 7 8 9 
  // ]
  // combinatevectors ( x , y , z ) 
  // // Example #3 
  // x = [1 2;3 4]; 
  // y = [5 6;7 8]; 
  // expected = [
  //   1 1 2 2 
  //   3 3 4 4 
  //   5 6 5 6 
  //   7 8 7 8 
  // ]
  // combinatevectors ( x , y ) 
  // 
  // Authors 
  //   Yann Collette 
  //   Pierre Lando 
  //   Michael Baudin 
  // 
  
  [lhs,rhs]=argn(); 
  // Check that all arguments are either row vectors or column vectors 
  areallrows = %t; 
  areallcolumns = %t; 
  for i = 1:rhs 
    X = varargin(i); 
    if (size(X,1) <> 1) then 
      areallrows = %f; 
    end 
    if (size(X,2) <> 1) then 
      areallcolumns = %f; 
    end 
  end 
  if (~areallrows) & (~areallcolumns) then 
    error ( msprintf ( gettext ( "%s: All arguments must be either row or column vectors."),"combinatevectors")) 
  end 
  if rhs == 1 then 
    R = X 
    return 
  end 
  if areallrows then 
    R = combinatevectors_2args ( varargin(1) , varargin(2) ); 
    for i = 3:rhs 
      R = combinatevectors_2args ( R , varargin(i) ); 
    end 
  else 
    R = combinatevectors_2args ( varargin(1).' , varargin(2).' ); 
    for i = 3:rhs 
      R = combinatevectors_2args ( R , varargin(i).' ); 
    end 
    R = R.' 
  end 
endfunction 
function R = combinatevectors_2args ( X , Y ) 
  cx=size(X,2); 
  cy=size(Y,2); 
  R=[X .*. ones(1,cy); ones(1,cx) .*. Y]; 
endfunction 

// 
// assert_close -- 
//   Returns 1 if the two real matrices computed and expected are close, 
//   i.e. if the relative distance between computed and expected is lesser than epsilon. 
// Arguments 
//   computed, expected : the two matrices to compare 
//   epsilon : a small number 
// 
function flag = assert_close ( computed, expected, epsilon ) 
 if expected==0.0 then 
   shift = norm(computed-expected); 
 else 
   shift = norm(computed-expected)/norm(expected); 
 end 
 if shift < epsilon then 
   flag = 1; 
 else 
   flag = 0; 
 end 
 if flag <> 1 then pause,end 
endfunction 
// 
// assert_equal -- 
//   Returns 1 if the two real matrices computed and expected are equal. 
// Arguments 
//   computed, expected : the two matrices to compare 
//   epsilon : a small number 
// 
function flag = assert_equal ( computed , expected ) 
 if computed==expected then 
   flag = 1; 
 else 
   flag = 0; 
 end 
 if flag <> 1 then pause,end 
endfunction 

x = [1 2 3]; 
y = [4 5 6]; 
computed = combinatevectors ( x , y ); 
expected = [ 
   1.    1.    1.    2.    2.    2.    3.    3.    3.    
   4.    5.    6.    4.    5.    6.    4.    5.    6. 
];
assert_equal ( computed , expected ); 


x = [1 2 3]; 
y = [4 5 6]; 
z = [7 8 9]; 
computed = combinatevectors ( x , y , z ); 
expected = [ 
   1.    1.    1.    1.    1.    1.    1.    1.    1.    2.    2.    2.    2.    2.    2.    2.    2.    2.    3.    3.    3.    3.    3.    3.    3.    3.    3.    
   4.    4.    4.    5.    5.    5.    6.    6.    6.    4.    4.    4.    5.    5.    5.    6.    6.    6.    4.    4.    4.    5.    5.    5.    6.    6.    6.    
   7.    8.    9.    7.    8.    9.    7.    8.    9.    7.    8.    9.    7.    8.    9.    7.    8.    9.    7.    8.    9.    7.    8.    9.    7.    8.    9. 
];
assert_equal ( computed , expected ); 

x = [1 2;3 4]; 
y = [5 6;7 8]; 
cmd = "computed = combinatevectors ( x , y );"; 
execstr(cmd,"errcatch"); 
computed = lasterror(); 
expected = "combinatevectors: All arguments must be either row or column vectors."; 
assert_equal ( computed , expected ); 

x = [ 
1 
2 
3 
]; 
y = [ 
4 
5 
6 
]; 
computed = combinatevectors ( x , y ); 
expected = [ 
   1    4    
   1    5    
   1    6    
   2    4    
   2    5    
   2    6    
   3    4    
   3    5    
   3    6 
];
assert_equal ( computed , expected ); 

x = [1;2;3]; 
y = [4;5;6]; 
z = [7;8;9];
computed = combinatevectors ( x , y , z ); 
expected = [ 
1 4 7
1 4 8
1 4 9
1 5 7
1 5 8
1 5 9
1 6 7
1 6 8
1 6 9
2 4 7
2 4 8
2 4 9
2 5 7
2 5 8
2 5 9
2 6 7
2 6 8
2 6 9
3 4 7
3 4 8
3 4 9
3 5 7
3 5 8
3 5 9
3 6 7
3 6 8
3 6 9
];
assert_equal ( computed , expected ); 



