Documentation : Introduction to Discrete Probabilities with Scilab

Abstract

In this article, we present an introduction to discrete probabilities 
with Scilab. Numerical experiments are based on Scilab. The first section 
presents discrete random variables and conditionnal probabilities. In the 
second section, we present combinations problems, tree diagrams and 
Bernouilli trials. In the third section, we present simulation of random 
processes with Scilab. 

Author

Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin

Licence

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :
http://creativecommons.org/licenses/by-sa/3.0/


